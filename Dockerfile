FROM ruby

MAINTAINER Mohamed El Jemai (mohamed.el.jemai@rwth-aachen.de)

RUN apt -qq update && \
	apt install rubygems nodejs -y &&\
	gem install rails && \
	rails new helloworld

WORKDIR helloworld

EXPOSE 3000

CMD  bin/rails server -b 0.0.0.0

